#[derive(Copy, Clone)]
pub enum PicieType{
    Pawn,
    Rook,
    Knight,
    Bishop,
    Queen,
    King
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub enum PicieColor{
    Black,
    White
}

#[derive(Copy, Clone)]
pub struct Position {
    pub x: u8,
    pub y: u8
}

impl Position {
    pub fn new(x: u8, y: u8) -> Position{
        Position {
            x: x,
            y: y
        }
    }
}

fn emp_moves() -> [[bool; 8]; 8] {
    [[false; 8]; 8]
}

#[derive(Copy, Clone)]
pub struct Picie{
    pub position: Position,
    pub moves:    [[bool; 8]; 8],
    pub color:    PicieColor,
    pub variant:  PicieType
}

impl Picie {
    pub fn new(x: u8, y: u8, color: PicieColor, p_type: PicieType) -> Picie{
        Picie{
            position: Position::new(x, y),
            moves:    emp_moves(),
            color:    color,
            variant:  p_type
        }
    }

    fn set_true(&mut self, x: u8, y: u8){
        if x < 8 && y < 8 {
            self.moves[y as usize][x as usize] = true;
        }
    }
    fn set_false(&mut self, x: u8, y: u8){
        if x < 8 && y < 8 {
            self.moves[y as usize][x as usize] = false;
        }
    }

    pub fn make_turn(&mut self, x: u8, y: u8){
        self.move_list();
        if self.moves[y as usize][x as usize] {
            self.position.x = x;
            self.position.y = y;
        }
    }

    fn mirror_set_true(&mut self, dx: u8, dy: u8) {
        match self.position.x.checked_add(dx){
            Some(x) => match self.position.y.checked_add(dy){
                Some(y) => self.set_true(x, y),
                _  => ()
            },
            _  => ()
        }
        match self.position.x.checked_add(dx){
            Some(x) => match self.position.y.checked_sub(dy){
                Some(y) => self.set_true(x, y),
                _  => ()
            },
            _  => ()
        }
        match self.position.x.checked_sub(dx){
            Some(x) => match self.position.y.checked_add(dy){
                Some(y) => self.set_true(x, y),
                _  => ()
            },
            _  => ()
        }
        match self.position.x.checked_sub(dx){
            Some(x) => match self.position.y.checked_sub(dy){
                Some(y) => self.set_true(x, y),
                _  => ()
            },
            _  => ()
        }
    }
    pub fn commrade_position_set_false(&mut self, picies: &Vec<Picie>){
        for picie in picies{
            if picie.color == self.color {
                self.set_false(picie.position.x, picie.position.y);
            }
        }
    }

    pub fn move_with_cutdown_avaible(&mut self, picies: &Vec<Picie>){
        match self.variant{
            PicieType::Pawn => {
                match self.color {
                    PicieColor::Black => {
                        match self.position.y.checked_sub(1) {
                            Some(y) => {
                                match self.position.x.checked_sub(1){
                                    Some(x) => {
                                        self.set_false(x, y);
                                        for picie in picies{
                                            if (picie.position.x == x)&&
                                               (picie.position.y == y){
                                               self.set_true(x, y); 
                                            };
                                        }
                                    },
                                    _ => ()
                                }
                                match self.position.x.checked_add(1){
                                    Some(x) => {
                                        self.set_false(x, y);
                                        for picie in picies{
                                            if (picie.position.x == x) &&
                                               (picie.position.y == y){
                                               self.set_true(x, y); 
                                            };
                                        }
                                    },
                                    _ => ()
                                }
                            },
                            _ => ()
                        }
                    },
                    PicieColor::White => {
                        match self.position.y.checked_add(1) {
                            Some(y) => {
                                match self.position.x.checked_sub(1){
                                    Some(x) => {
                                        self.set_false(x, y);
                                        for picie in picies{
                                            if (picie.position.x == x) &&
                                               (picie.position.y == y){
                                               self.set_true(x, y); 
                                            };
                                        }
                                    },
                                    _ => ()
                                }
                                match self.position.x.checked_add(1){
                                    Some(x) => {
                                        self.set_false(x, y);
                                        for picie in picies{
                                            if (picie.position.x == x) &&
                                               (picie.position.y == y){
                                               self.set_true(x, y); 
                                            };
                                        }
                                    },
                                    _ => ()
                                }

                            },
                            _ => ()
                        }
                    }
                }
            }
            _ => ()
        }
    }

    pub fn vector_move_list(&mut self, picies: &Vec<Picie>) {
        match self.variant {
            PicieType::Rook => {
                for x in self.position.x..8 {
                    let mut stop = false;
                    for picie in picies{
                        self.set_true(x, self.position.y);
                        if picie.position.x == x && picie.position.y == self.position.y{
                            stop = true;
                            break;
                        }
                    }
                    if stop {
                        break;   
                    }
                }
                for x in (0..self.position.x).rev() {
                    let mut stop = false;
                    for picie in picies{
                        self.set_true(x, self.position.y);
                        if picie.position.x == x && picie.position.y == self.position.y{
                            stop = true;
                            break;
                        }
                    }
                    if stop {
                        break;   
                    }
                }
                for y in self.position.y..8 {
                    let mut stop = false;
                    for picie in picies{
                        self.set_true(self.position.x, y);
                        if picie.position.x == self.position.x && picie.position.y == y{
                            stop = true;
                            break;
                        }
                    }
                    if stop {
                        break;   
                    }
                }
                for y in (0..self.position.y).rev() {
                    let mut stop = false;
                    for picie in picies{
                        self.set_true(self.position.x, y);
                        if picie.position.x == self.position.x && picie.position.y == y{
                            stop = true;
                            break;
                        }
                    }
                    if stop {
                        break;   
                    }
                }
            },
            PicieType::Bishop => {
                for x in self.position.x..8 {
                    let mut stop = false;
                    for y in self.position.y..8{
                        for picie in picies{
                            let x_dif = && (&(self.position.x as i8) - x as i8).abs();
                            let y_dif = && (&(self.position.y as i8) - y as i8).abs();
                            if x_dif == y_dif  {
                                self.set_true(x, y);
                            }
                            if picie.position.x == x && picie.position.y == y{
                                stop = true;
                                break;
                            }
                        }
                        if stop {
                            break;   
                        }
                    }
                    if stop {
                        break;   
                    }
                }
                for x in self.position.x..8 {
                    let mut stop = false;
                    for y in (0..self.position.y).rev(){
                        for picie in picies{
                            let x_dif = && (&(self.position.x as i8) - x as i8).abs();
                            let y_dif = && (&(self.position.y as i8) - y as i8).abs();
                            if x_dif == y_dif  {
                                self.set_true(x, y);
                            }
                            if picie.position.x == x && picie.position.y == y{
                                stop = true;
                                break;
                            }
                        }
                        if stop {
                            break;   
                        }
                    }
                    if stop {
                        break;   
                    }
                }
                for x in (0..self.position.x).rev() {
                    let mut stop = false;
                    for y in self.position.y..8{
                        for picie in picies{
                            let x_dif = && (&(self.position.x as i8) - x as i8).abs();
                            let y_dif = && (&(self.position.y as i8) - y as i8).abs();
                            if x_dif == y_dif  {
                                self.set_true(x, y);
                            }
                            if picie.position.x == x && picie.position.y == y{
                                stop = true;
                                break;
                            }
                        }
                        if stop {
                            break;   
                        }
                    }
                    if stop {
                        break;   
                    }
                }
                for x in (0..self.position.x).rev() {
                    let mut stop = false;
                    for y in (0..self.position.y).rev(){
                        for picie in picies{
                            let x_dif = && (&(self.position.x as i8) - x as i8).abs();
                            let y_dif = && (&(self.position.y as i8) - y as i8).abs();
                            if x_dif == y_dif  {
                                self.set_true(x, y);
                            }
                            if picie.position.x == x && picie.position.y == y{
                                stop = true;
                                break;
                            }
                        }
                        if stop {
                            break;   
                        }
                    }
                    if stop {
                        break;   
                    }
                }
            },
            PicieType::Queen => {
                for x in self.position.x..8 {
                    let mut stop = false;
                    for y in self.position.y..8{
                        for picie in picies{
                            let x_dif = && (&(self.position.x as i8) - x as i8).abs();
                            let y_dif = && (&(self.position.y as i8) - y as i8).abs();
                            if x_dif == y_dif  {
                                self.set_true(x, y);
                            }
                            if picie.position.x == x && picie.position.y == y{
                                stop = true;
                                break;
                            }
                        }
                        if stop {
                            break;   
                        }
                    }
                    if stop {
                        break;   
                    }
                }
                for x in self.position.x..8 {
                    let mut stop = false;
                    for y in (0..self.position.y).rev(){
                        for picie in picies{
                            let x_dif = && (&(self.position.x as i8) - x as i8).abs();
                            let y_dif = && (&(self.position.y as i8) - y as i8).abs();
                            if x_dif == y_dif  {
                                self.set_true(x, y);
                            }
                            if picie.position.x == x && picie.position.y == y{
                                stop = true;
                                break;
                            }
                        }
                        if stop {
                            break;   
                        }
                    }
                    if stop {
                        break;   
                    }
                }
                for x in (0..self.position.x).rev() {
                    let mut stop = false;
                    for y in self.position.y..8{
                        for picie in picies{
                            let x_dif = && (&(self.position.x as i8) - x as i8).abs();
                            let y_dif = && (&(self.position.y as i8) - y as i8).abs();
                            if x_dif == y_dif  {
                                self.set_true(x, y);
                            }
                            if picie.position.x == x && picie.position.y == y{
                                stop = true;
                                break;
                            }
                        }
                        if stop {
                            break;   
                        }
                    }
                    if stop {
                        break;   
                    }
                }
                for x in (0..self.position.x).rev() {
                    let mut stop = false;
                    for y in (0..self.position.y).rev(){
                        for picie in picies{
                            let x_dif = && (&(self.position.x as i8) - x as i8).abs();
                            let y_dif = && (&(self.position.y as i8) - y as i8).abs();
                            if x_dif == y_dif  {
                                self.set_true(x, y);
                            }
                            if picie.position.x == x && picie.position.y == y{
                                stop = true;
                                break;
                            }
                        }
                        if stop {
                            break;   
                        }
                    }
                    if stop {
                        break;   
                    }
                }
                for x in self.position.x..8 {
                    let mut stop = false;
                    for picie in picies{
                        self.set_true(x, self.position.y);
                        if picie.position.x == x && picie.position.y == self.position.y{
                            stop = true;
                            break;
                        }
                    }
                    if stop {
                        break;   
                    }
                }
                for x in (0..self.position.x).rev() {
                    let mut stop = false;
                    for picie in picies{
                        self.set_true(x, self.position.y);
                        if picie.position.x == x && picie.position.y == self.position.y{
                            stop = true;
                            break;
                        }
                    }
                    if stop {
                        break;   
                    }
                }
                for y in self.position.y..8 {
                    let mut stop = false;
                    for picie in picies{
                        self.set_true(self.position.x, y);
                        if picie.position.x == self.position.x && picie.position.y == y{
                            stop = true;
                            break;
                        }
                    }
                    if stop {
                        break;   
                    }
                }
                for y in (0..self.position.y).rev() {
                    let mut stop = false;
                    for picie in picies{
                        self.set_true(self.position.x, y);
                        if picie.position.x == self.position.x && picie.position.y == y{
                            stop = true;
                            break;
                        }
                    }
                    if stop {
                        break;   
                    }
                }
            } 
            _ => ()
        } 
    }
    
    pub fn move_list(&mut self){
        let x = self.position.x;
        let y = self.position.y;
        match self.variant {
            PicieType::Pawn => {
                match self.color {
                    PicieColor::Black => {
                        match y.checked_sub(1) {
                            Some(yt) => {
                                self.set_true(x, yt);
                                match x.checked_sub(1) {
                                    Some(xt) => self.set_true(xt, yt),
                                    _ => () 
                                }
                                match x.checked_add(1) {
                                    Some(xt) => self.set_true(xt, yt),
                                    _ => () 
                                }
                            },
                            _ => ()
                        }
                        self.set_true(x, y);
                    },
                    PicieColor::White => {
                        match y.checked_add(1) {
                            Some(yt) => {
                                self.set_true(x, yt);
                                match x.checked_sub(1) {
                                    Some(xt) => self.set_true(xt, yt),
                                    _ => () 
                                }
                                match x.checked_add(1) {
                                    Some(xt) => self.set_true(xt, yt),
                                    _ => () 
                                }
                            },
                            _ => ()
                        }
                        self.set_true(x, y);
                    }
                }               
            },
            PicieType::Knight => {
               self.mirror_set_true(2, 1); 
               self.mirror_set_true(1, 2); 
            }
            _ => {
                
            }
        }
    }
    pub fn print_field(self){
        println!("  A B C D E F G H ");
        println!(" ╔════════════════╗");
        for i in (0..8).rev() {
            print!("{}║", i);
            for j in 0..8 {
                if self.position.x as usize == j as usize && self.position.y as usize == i as usize {
                    print!("{} ", match self.color {
                        PicieColor::White => match self.variant{
                            PicieType::Pawn     => "♙",
                            PicieType::Knight   => "♘",
                            PicieType::Rook     => "♖",
                            PicieType::Bishop   => "♗",
                            PicieType::King     => "♔",
                            PicieType::Queen    => "♕"
                        },
                        PicieColor::Black => match self.variant{
                            PicieType::Pawn     => "♟",
                            PicieType::Knight   => "♞",
                            PicieType::Rook     => "♜",
                            PicieType::Bishop   => "♝",
                            PicieType::King     => "♚",
                            PicieType::Queen    => "♛"
                        }
                    })
                }
                else if self.moves[i][j] {
                    print!("▓▓");
                }
                else {
                    print!("░░");
                }
            }
            println!("║{}", i);
        }
        println!(" ╚════════════════╝");
        println!("  A B C D E F G H ");
    }
}

#[test]
fn test_move_white() {
    let mut picie = Picie::new(1, 1, PicieColor::White, PicieType::Pawn);
    picie.move_list();
    assert_eq!(picie.moves[1][1] && picie.moves[2][1] && picie.moves[2][0], true);
}

#[test]
fn test_move_black() {
    let mut picie = Picie::new(1, 6, PicieColor::Black, PicieType::Pawn);
    picie.move_list();
    assert_eq!(picie.moves[6][1] && picie.moves[5][1] && picie.moves[5][0], true);
}
#[test]
fn test_turning() {
    let mut picie = Picie::new(1, 6, PicieColor::Black, PicieType::Pawn);
    picie.make_turn(1, 5);
    picie.move_list();
    assert_eq!(picie.moves[4][1], true);
}
#[test]
fn test_turning_false() {
    let mut picie = Picie::new(1, 6, PicieColor::Black, PicieType::Pawn);
    picie.make_turn(1, 2);
    picie.move_list();
    assert_eq!(picie.moves[4][1], false);
}

#[test]
fn test_knight(){
    let mut knight = Picie::new(1, 6, PicieColor::Black, PicieType::Knight);
    knight.make_turn(2, 4);  
    knight.move_list();
    assert_eq!(knight.moves[6][1], true);
}

#[test]
fn test_commrad_position(){
    let mut pawn1 = Picie::new(1, 1, PicieColor::White, PicieType::Pawn);
    let pawn2 = Picie::new(1, 2, PicieColor::White, PicieType::Pawn);
    pawn1.move_list();
    pawn1.commrade_position_set_false(&vec![pawn2]);
    assert_eq!(pawn1.moves[2][1], false);
}

#[test]
fn test_move_with_cut_down(){
    let mut picie = Picie::new(1, 6, PicieColor::Black, PicieType::Pawn);
    let mut picie1 = Picie::new(5, 6, PicieColor::Black, PicieType::Pawn);
    let picies = vec![Picie::new(0, 5, PicieColor::White, PicieType::Pawn),
                                Picie::new(2, 5, PicieColor::White, PicieType::Pawn)];
    picie.move_list();
    picie.commrade_position_set_false(&picies);
    picie.move_with_cutdown_avaible(&picies);
    picie1.move_list();
    picie1.commrade_position_set_false(&picies);
    picie1.move_with_cutdown_avaible(&picies);
    picie.print_field();
    assert_eq!(picie.moves[5][2] && picie.moves[5][0] && !picie1.moves[5][4], true);
}

#[test]
fn test_rook(){
    let mut picie = Picie::new(3, 3, PicieColor::White, PicieType::Rook);
    picie.vector_move_list(&vec![Picie::new(5, 3, PicieColor::Black, PicieType::Pawn)]);
    picie.print_field();
    assert_eq!(picie.moves[3][2] && picie.moves[0][3] && picie.moves[3][4] && !picie.moves[4][6] && !picie.moves[3][6], true);
}

#[test]
fn test_bishop(){
    let mut picie = Picie::new(3, 3, PicieColor::White, PicieType::Bishop);
    picie.vector_move_list(&vec![Picie::new(4, 4, PicieColor::Black, PicieType::Pawn)]);
    picie.print_field();
    assert_eq!(picie.moves[4][4] && !picie.moves[5][5], true);
}

#[test]
fn test_queen(){
    let mut picie = Picie::new(3, 3, PicieColor::White, PicieType::Queen);
    picie.vector_move_list(&vec![Picie::new(4, 4, PicieColor::Black, PicieType::Pawn)]);
    picie.print_field();
    assert_eq!(picie.moves[4][4] && !picie.moves[5][5], true);
}