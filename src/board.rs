use crate::pieces::Picie;
use crate::pieces::PicieColor::*;
use crate::pieces::PicieType::*;

pub struct Board {
    pub field: Vec<Picie>
}

impl Board {
    pub fn new() -> Board{
        Board{field: Vec::new()}
    }

    pub fn new_pawn() -> Board{
        let mut field = Vec::new();
        for i in 0..8{
            field.push(Picie::new(i as u8, 1, White, Pawn));
            field.push(Picie::new(i as u8, 6, Black, Pawn));
        }
        Board{field: field}
    }

    pub fn new_pawn_knight() -> Board{
        let mut field = Vec::new();
        for i in 0..8{
            field.push(Picie::new(i as u8, 1, White, Pawn));
            field.push(Picie::new(i as u8, 6, Black, Pawn));
        }
        field.push(Picie::new(1, 7, Black, Knight));
        field.push(Picie::new(6, 7, Black, Knight));
        field.push(Picie::new(1, 0, White, Knight));
        field.push(Picie::new(6, 0, White, Knight));
        Board{field: field}
    }

    pub fn new_classic_board() -> Board{
        let mut field = Vec::new();
        for i in 0..8{
            field.push(Picie::new(i as u8, 1, White, Pawn));
            field.push(Picie::new(i as u8, 6, Black, Pawn));
        }
        field.push(Picie::new(0, 7, Black, Rook));
        field.push(Picie::new(1, 7, Black, Knight));
        field.push(Picie::new(2, 7, Black, Bishop));
        field.push(Picie::new(3, 7, Black, Queen));
        field.push(Picie::new(4, 7, Black, King));
        field.push(Picie::new(5, 7, Black, Bishop));
        field.push(Picie::new(6, 7, Black, Knight));
        field.push(Picie::new(7, 7, Black, Rook));
        field.push(Picie::new(0, 0, White, Rook));
        field.push(Picie::new(1, 0, White, Knight));
        field.push(Picie::new(2, 0, White, Bishop));
        field.push(Picie::new(3, 0, White, Queen));
        field.push(Picie::new(4, 0, White, King));
        field.push(Picie::new(5, 0, White, Bishop));
        field.push(Picie::new(6, 0, White, Knight));
        field.push(Picie::new(7, 0, White, Rook));
        Board{field: field}
    }

    pub fn get_by_x_y(&self, x: u8, y: u8) -> (bool, usize){
        let mut picie = 65;
        for i in 0..self.field.len(){
            if self.field[i].position.x == x && self.field[i].position.y == y{
                picie = i;
            }
        }
        match picie{
            x if x < 65 => (true, x),
            _ => (false, 0usize)
        }
    }

    pub fn make_turn(&mut self, x1: u8, y1: u8, x2: u8, y2: u8) -> bool{
        let picie = self.get_by_x_y(x1, y1);
        match picie {
            (true, a) => self.field[a].make_turn(x2, y2),
            (false, _) => ()
        }
        true
    }

    pub fn print_field(&self) {
        println!("________________");
        println!("  A B C D E F G H ");
        println!(" ╔════════════════╗");
        for i in (0..8).rev(){
            print!("{}║", i);
            for j in 0..8{
                let a = self.get_by_x_y(j, i);
                match a {
                    (true, x) => {
                        print!("{} ", match self.field[x].color {
                            White => match self.field[x].variant{
                                Pawn     => "♙",
                                Knight   => "♘",
                                Rook     => "♖",
                                Bishop   => "♗",
                                King     => "♔",
                                Queen    => "♕"
                            },
                            Black => match self.field[x].variant{
                                Pawn     => "♟",
                                Knight   => "♞",
                                Rook     => "♜",
                                Bishop   => "♝",
                                King     => "♚",
                                Queen    => "♛"
                            }
                        })
                    },
                    (false, _) => if (i as u8 + j as u8) % 2 == 1_u8 {
                        print!("▓▓");
                    }
                    else {
                        print!("░░");
                    }
                }
            }
            println!("║{}", i);
        }
        println!(" ╚════════════════╝");
        println!("  A B C D E F G H ");
        println!("________________");
    }
}

#[test]
fn test_board_with_pawns() {
    let board = Board::new_pawn();
    board.print_field();
    assert_eq!(board.get_by_x_y(0, 1), (true, 0));
}
#[test]
fn test_board_turn() {
    let mut board = Board::new_pawn();
    board.print_field();
    board.make_turn(0, 1, 0, 2);
    board.print_field();
    assert_eq!(board.get_by_x_y(0, 2), (true, 0));
}
#[test]
fn test_knight_turn() {
    let mut board = Board::new_pawn_knight();
    board.print_field();
    board.make_turn(1, 0, 0, 2);
    board.print_field();
    assert_eq!(board.get_by_x_y(0, 2), (true, 18));
}
#[test]
fn test_board_start() {
    let mut board = Board::new_classic_board();
    board.print_field();
    board.make_turn(1, 0, 0, 2);
    board.make_turn(3, 6, 3, 5);
    board.print_field();
}